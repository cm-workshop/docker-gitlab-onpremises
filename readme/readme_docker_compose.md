# Docker Compose script for running as container

1. Create `docker-compose.yml` file

1.1 Defaul configuration

for default configuration need to config the `gitlab.rb` later
on `/etc/gitlab/gitlab.rb` to adding `external_url` key

```yaml
version: "3.8"

services:
  gitlab: # GITLAB CONTAINER
    image: gitlab/gitlab-ce:latest
    container_name: gitlab
    restart: always
    ports:
      - "443:443"
      - "80:80"
      - "22:22"
    volumes:
      - $GITLAB_HOME/config:/etc/gitlab
      - $GITLAB_HOME/logs:/var/log/gitlab
      - $GITLAB_HOME/data:/var/opt/gitlab
    shm_size: 4096m
    networks:
      - onpremises-net
  registry: # PRIVATE REGISTRY CONTAINER
    image: registry:latest
    container_name: private_registry
    ports:
      - "$IP_ADDRESS:5000:5000"
    volumes:
      - $DOCKER_REGISTRY/data:/var/lib/registry
    environment:
      REGISTRY_HTTP_ADDR: 0.0.0.0:5000
    networks:
      - onpremises-net

networks:
  onpremises-net:
    driver: bridge
```

1.2 Pre-configuration (environment configuration)

## Final `docker-compose.yml` file with gitlab container + private registry container

need to config `external_url` to `http://$IP_ADDRESS/` / `https://$IP_ADDRESS/`

```yaml
version: "3.8"

services:
  gitlab:
    image: gitlab/gitlab-ce:latest
    container_name: gitlab
    environment:
      GITLAB_OMNIBUS_CONFIG: "external_url 'http://$IP_ADDRESS/'; gitlab_rails['lfs_enabled'] = true;"
    restart: always
    ports:
      - "443:443"
      - "80:80"
      - "22:22"
    volumes:
      - $GITLAB_HOME/config:/etc/gitlab
      - $GITLAB_HOME/logs:/var/log/gitlab
      - $GITLAB_HOME/data:/var/opt/gitlab
    dns: 8.8.8.8
    shm_size: 4096m
    networks:
      - onpremises-net
  registry:
    image: registry:latest
    container_name: private_registry
    restart: always
    ports:
      - "$IP_ADDRESS:5000:5000"
    volumes:
      - $DOCKER_REGISTRY/data:/var/lib/registry
    environment:
      REGISTRY_HTTP_ADDR: 0.0.0.0:5000
    networks:
      - onpremises-net

networks:
  onpremises-net:
    driver: bridge
```

2. Prepare to running `docker compose up -d`:

- Create `.env` for `docker-compose.yml`

macOS/Linux:

```yaml
IP_ADDRESS=127.0.0.1
GITLAB_HOME=/srv/gitlab
DOCKER_REGISTRY=/srv/docker-registry
```

Windows:

```yaml
IP_ADDRESS=127.0.0.1
GITLAB_HOME=./gitlab
DOCKER_REGISTRY=./docker-registry
```

- Create bash script to run `nano commands-build`

macOS/Linux:

```bash
export IP_ADDRESS=...
export GITLAB_HOME=/srv/gitlab
export DOCKER_REGISTRY=/srv/docker-container-registry
docker compose up -d
```

Windows:

```bash
set IP_ADDRESS=...
set GITLAB_HOME=%cd%/gitlab
set DOCKER_REGISTRY=%cd%/docker-registry
docker compose up -d
```
